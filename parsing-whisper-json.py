import sys
import json
import csv

def load_json_file(file_path):
    try:
        with open(file_path, 'r') as json_file:
            data = json.load(json_file)
        return data
    except FileNotFoundError:
        print(f"The file '{file_path}' was not found.")
        return None
    except json.JSONDecodeError:
        print(f"Error decoding JSON in '{file_path}'. Please check the file contents.")
        return None

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script_name.py <json_file>")
        exit()
    
    file_path = sys.argv[1]
    json_data = load_json_file(file_path)

    if json_data:
        segments = json_data["segments"]

        # initial csv data with headers
        csv_data = [["Word", "Start", "End", "Confidence"]]

        # File path for the CSV file
        csv_file_path = 'data.csv'

        for segment in segments:
            words = segment["words"]
            for word in words:
                csv_data.append([word["text"], word["start"], word["end"], word["confidence"]])
        
        # Writing data to the CSV file
        with open(csv_file_path, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerows(csv_data)

    print(f'Data has been written to {csv_file_path}')
    
        