# pip3 install openai-whisper
# pip3 install numpy==1.25
import whisper

# pip3 install git+https://github.com/linto-ai/whisper-timestamped
import whisper_timestamped

import io
import os
import json

# pip3 install pandas
import pandas as pd

model = whisper.load_model("base")

audio_file = "first_6.wav"

print("Audio file:", audio_file)

result = whisper_timestamped.transcribe(model, audio = audio_file)

json_data = json.dumps(result)

# write the JSON string to a file
with open('data.json', 'w') as f:
    f.write(json_data)

